<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('grocery_CRUD');
	}

	// public function index($output = null)
	// {
	// 	$this->load->view('template/header_admin', $output);
	//     $this->load->view('pages/admin', $output);
	//     $this->load->view('template/footer', $output);
	// }

	public function output($output = null)
	{
		$this->load->view('template/header_admin', (array) $output);
		$this->load->view('pages/admin', (array) $output);
		$this->load->view('template/footer_admin', (array) $output);
	}



	public function users()
	{
		if ($_SESSION['userdata']['logged_in'] === true) {
			try {
				$crud = new grocery_CRUD();

				$crud->set_theme('datatables');
				$crud->set_table('user');
				$crud->set_subject('User');
				//* $crud->columns('id','username','email','role','actif');
				//! 
				
				//todo callback_delete !!!!!!!!!!!!
				$crud->callback_delete(array($this, 'delete_user'));
				//*var_dump( $crud->getState());
				//? var_dump($crud->getStateInfo());
				$output = $crud->render();
				$this->output($output);
				

			} catch (Exception $e) {
				show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
			}
		} else {
			unset($_SESSION['userdata']);
			redirect('/connexion');
		}
	}

	public function article()
	{ //! load data infini => a réparer / reprendre
	//*	$this->form_validation->set_rules('titre', 'Titre', 'required');
       //* $this->form_validation->set_rules('texte', 'Texte', 'required');

		
		if ($_SESSION['userdata']['logged_in'] === true) {
			
			try {
				$crud = new grocery_CRUD();
				//* var_dump($crud->getState());
				$crud->set_theme('datatables');
				$crud->set_table('articles');
				$crud->set_subject('Article');
				$crud->unset_add();

				$output = $crud->render();
				//* var_dump($output);
				$this->output($output);
			} catch (Exception $e) {
				show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
			}
		}else{
			unset($_SESSION['userdata']);
			redirect('/connexion');
		}
	}

	public function delete_user($primary_key)
	{
		return $this->db->update('user', array('actif' => false), array('id' => $primary_key));
	}

public function addArticle()
{
	//* var_dump('addArticle');
	$this->load->view('template/header_admin');
	$this->load->view('pages/article');
}

	public function logout()
	{
		unset($_SESSION['userdata']);
			redirect('/');
	}

	function article_insert_callback($post_array)
    {
       
        $this->db->insert('articles', $post_array);
        redirect('/admin/article', 'auto', NULL);
        //todo message success !!

	}
	
	public function insertArticle()
	{
		//? var_dump($_POST);
		$crud = new grocery_CRUD();
		$crud->set_table('articles');

		//*$crud->article_insert_callback();
		$crud->callback_insert(array($this, 'article_insert_callback'));
		$crud->render();
	}
}


