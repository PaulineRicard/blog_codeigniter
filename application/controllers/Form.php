<?php
include_once("/home/pauline/ServeurWeb/codeigniter/system/libraries/Form_validation.php");

class Form extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
      
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
    }


    public function add()
    {
       
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        //? var_dump($this->input->post('myform'));
        if ($this->form_validation->run() == FALSE) {
            //todo voir message d'erreur !!!
           
            redirect('/inscription');
        } else {

            try {
                $crud = new grocery_CRUD();
                $crud->set_table('user');
                $crud->callback_insert(array($this, 'user_insert_callback'));
                $crud->render();
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        }
    }
    function user_insert_callback($post_array)
    {
        //todo crypt password
        //*$this->load->library('encrypt');
        //*$key = 'super-secret-key';
        //*$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
        $this->db->insert('user', $post_array);
        redirect('/connexion', 'auto', NULL);
        //todo message success !!

    }

    public function connect()
    {
        //todo form_validation
        //? var_dump($_POST);
        $condition = "username =" . "'" . $_POST['username'] . "' AND " . "password =" . "'" . $_POST['password'] . "'";
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            //todo session!!
            $user = $query->result()[0];
            $newdata = array(
                'username'  => $user->username,
                'email'     => $user->email,
                'role'      => $user->role,
                'logged_in' => TRUE
            );
            $_SESSION['userdata']= $newdata;
            $this->login($user);
   //! ####################################  message d'erreur a mettre en place $_SESSION marche pas!!!!!!          OK !!
        } else {
            $message= 'Identifiants incorrects';
            $_SESSION['message'] = $message;
            $this->session->mark_as_flash('message');
            $this->load->view('template/header');
            $this->load->view('pages/connexion');
        }
    }

    public function login($user)
    {
        //?var_dump('connexion ok!!');
        //? var_dump($user);
        var_dump($_SESSION);
        if ($user->role === 'admin' && $_SESSION['userdata']['logged_in'] === true) {
            //? var_dump('admin');
            //todo: redirect vers page account admin
            redirect('/admin');
        } else if($user->role === 'user' && $_SESSION['userdata']['logged_in'] === true){
            //? var_dump('user');
            //todo: redirect vers page account user
            redirect('/user');
        }else{
            unset($_SESSION["userdata"]);
        }
    }

    public function getRole()
    {
        if(isset($_SESSION['userdata']['role'])){
            echo $_SESSION['userdata']['role'];
        }else{
            echo 'false';
        }
    }


   
}
