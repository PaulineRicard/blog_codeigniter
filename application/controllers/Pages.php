<?php
//*require('DAO.php');
class Pages extends CI_Controller{
    
    
    public function __construct()
    {
        require('application/config/database.php');
        parent::__construct();
        $this->load->database($db);
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
    }

public function index()
{
    $query = $this->db->query("SELECT * FROM articles ORDER BY date_creation DESC limit 4");
    $tableSelect['rows'] = $query->result();
    $data['title'] = 'Home';
    $this->load->view('template/header', $data);
    $this->load->view('pages/home', $tableSelect);
    $this->load->view('template/footer');
    
}

    public function view($page)
    {
        // if ( ! file_exists('application/views/pages/'.$page.'.php'))
        // {
        //         // Whoops, we don't have a page for that!
        //         show_404();
        // }

        $data['title'] = ucfirst($page); // Capitalize the first letter
       //?  $data['dao'] = new DAO();    Voir si besoin ?????
        $this->load->view('template/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('template/footer', $data);
}


    
}