<div class="container mt-5 mb-5">
<h1 class="text-center">Nouvel Article</h1>

<div class="container d-flex justify-content-center">
<?php echo validation_errors(); ?>

<?php echo form_open('admin/article/insert'); ?>

<div class="form-group">
<h5>Titre</h5>
<input type="text" name="titre"  id="titre"  />
</div>

<div class="">
<h5>Texte</h5>
<textarea rows="5" cols="100" name="texte"  id="texte">
</textarea> 
</div>

<div class="form-group">
<h5>Lien Image</h5>
<input type="text" name="image"  id="image" />
</div>

<button type="submit"  class="btn btn-outline-success btn-lg">Ajouter</button>
<!-- <div><input type="submit" value="Submit" /></div> -->

</form>
</div>
</div> 

