

<div class="container mt-5 mb-5">
    <h1 class="text-center">Connexion</h1>
    <?php 
    if(isset($_SESSION['message'])){ ?>
        <div class="alert alert-danger mr-5 ml-5">
            <?php echo $_SESSION['message']; ?>
        </div>
     <?php  } ?>
    <div class="container d-flex justify-content-center">

        <form action="/connect" method="POST">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <button type="submit" class="btn btn-outline-success">se connecter</button>
        </form>
    </div>

</div>