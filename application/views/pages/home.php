<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1 class="display-3"><?= $title ?></h1>
		<p class="lead">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Amet culpa temporibus, laborum fugiat ipsam, id placeat excepturi voluptas dolorem sed repudiandae modi voluptates consequatur! Quam, aut. A deserunt totam nam?</p>
	</div>
</div>


<div class="container justify-content-center">
	<div class="row">

		<?php foreach ($rows as $row) { ?>
			<div class="card-group col-5">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title"><?= $row->titre ?></h4>
						<p class="card-text"><?= $row->texte ?></p>
						<?php if (isset($row->image)) { ?>
							<img src="<?= $row->image ?>" width="20%">

							<?php } ?>
							
							
						</div>
						
					</div>
				</div>
				<?php } ?>
			</div>
</div>