<!-- 
<div class="container mt-5 mb-5">
    <h1 class="text-center">Inscription</h1>
    <div class="container d-flex justify-content-center">
            <?php 
            //*$this->load->helper('form');
            //* form_open('signup', 'class="inscription" id="myform"')?>
    <form method="POST" action="<?= base_url('add/') ?>" id="myform"> 
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" >
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" >
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" >
        </div>
        <button type="submit"  class="btn btn-outline-success btn-lg">s'inscrire</button>
           </form>
    </div>
    
</div> -->
<div class="container mt-5 mb-5">
<h1 class="text-center">Inscription</h1>

<div class="container d-flex justify-content-center">
<?php echo validation_errors(); ?>

<?php echo form_open('insert'); ?>

<div class="form-group">
<label for="username">Username</label>
<input type="text" name="username"  id="username"  />
</div>

<div class="form-group">
<label for="email">Email</label>
<input type="text" name="email"  id="email"/>
</div>

<div class="form-group">
<label for="password">Password</label>
<input type="text" name="password"  id="password" />
</div>

<button type="submit"  class="btn btn-outline-success btn-lg">s'inscrire</button>
<!-- <div><input type="submit" value="Submit" /></div> -->

</form>
</div>
</div> 