<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if(isset($title)){ ?>
        <title><?= $title ?></title>
    <?php }
     if(isset($css_files)){
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach;  }?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="application/views/style.css"> -->
    
</head>

<body>

    <!-- navbar -->
    <div class="container">
        <div class="navbar container-fluid ">
            <nav class="navbar fixed-top navbar-expand-lg navbar-expand-md navbar-dark bg-dark">
                <!-- Navbar content -->

                <a class="navbar-brand" href="/">Home</a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                        <li class="nav-item">
                            <a class="nav-link" id ='inscription' href="/inscription">Inscription</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/connexion">Connexion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/users">Gerer Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/article">Gerer articles</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/article/add">Ajouter article</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="account">Mon Compte</a>
                        </li>
                        <li class="nav-item">
                            <?php if(isset($_SESSION['userdata']) && $_SESSION['userdata']['role'] === 'admin'){ ?>
                                <a class="nav-link" href="/admin/logout">Déconnexion</a>

                            <?php }else{ ?>
                                <a class="nav-link" href="#">Déconnexion</a>
                            <?php } ?> 
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- end navbar -->

    <!-- contenu de la page -->
    <div class="main mt-2">